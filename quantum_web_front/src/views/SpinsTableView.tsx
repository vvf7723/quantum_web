import React from "react";
import {Spins} from "../models/PlotsState";

interface SpinsTableViewProps {
    spins: Spins[]
}

export default function SpinsTableView({spins}:SpinsTableViewProps) {
    if(!spins || spins.length <=0){
        return null;
    }
    return <table className="SpinsTableView">
        <thead><tr><td colSpan={5}>Time from start</td></tr></thead>
        <tbody>
        {spins.map((spin, rowNumber) => <tr key={rowNumber}>
            <td>{Math.round(spin.time*1000)/1000}</td>
            {spin.vector.map((itemValue, columnNumber) =>
                <td
                    key={rowNumber + 'x' + columnNumber}
                    className={itemValue ? 'black' : ''}
                >
                    &nbsp;
                </td>
            )}
        </tr>)}
        </tbody>
    </table>
}