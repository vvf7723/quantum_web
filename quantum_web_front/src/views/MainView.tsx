import React from "react";
import EnergyPlotView from "./EnergyPlotView";
import startCalculation from "../business/startCalculation";
import {listenToTaskUpdates} from "../business/wsListener";
import PlotsState from "../models/PlotsState";
import SpinsTableView from "./SpinsTableView";

export default function MainView() {
    const [state, setState] = React.useState(new PlotsState());
    const start = async () => {
        try {
            const {taskId} = await startCalculation();
            await listenToTaskUpdates(taskId, new PlotsState(), setState);
        }catch (e) {
            alert("Ошибка - "+ e);
        }
    };
    return <div className="MainView">
        <p><button onClick={start} disabled={!state.isDone && state.plotData.length > 0}>Start calculation</button></p>
        <EnergyPlotView plotData={state.plotData}/>
        <SpinsTableView spins={state.spins}/>

    </div>
}