import {XYPlot, XAxis, YAxis, HorizontalGridLines, LineSeries} from 'react-vis';
import "react-vis/dist/style.css";
import {PlotData} from "../models/PlotsState";
import React from 'react';

interface EnergyPlotViewProps {
    plotData: PlotData[]
}

export default function EnergyPlotView({plotData}: EnergyPlotViewProps) {
    return <XYPlot
        width={800}
        height={600}
        className="EnergyPlotView"
    >
        <HorizontalGridLines/>
        <LineSeries
            curve={'curveMonotoneX'}
            data={plotData}
        />
        <XAxis/>
        <YAxis/>
        {plotData.length && <div>Energy = {plotData[plotData.length-1].y}</div>}
    </XYPlot>
}