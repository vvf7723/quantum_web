const {createProxyMiddleware:proxy} = require('http-proxy-middleware');
console.log(proxy, typeof proxy);

module.exports = function(app) {
  const apiHostName = process.env.API_HOST || 'localhost'
  const wsHostName = process.env.WS_HOST || 'localhost'
  app.use(proxy('/api/**', { target: 'http://'+apiHostName+':8000/' }));
  app.use(proxy('/ws/**', { target: 'http://'+wsHostName+':8754/', ws: true }));
};