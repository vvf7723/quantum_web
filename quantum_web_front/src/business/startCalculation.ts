export interface StartResult {
    taskId: string
}
export default async function startCalculation(): Promise<StartResult> {
    const response = await fetch('/api/start', {method:'POST'});
    return await response.json()
}