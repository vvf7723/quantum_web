function generateWSUrl(): string {

    let wsBaseUrl: string = '/ws/';
    if (process.env.NODE_ENV !== 'production') {
        wsBaseUrl = 'ws://localhost:8765/ws/'; // on production here will be just valid path
    }
    const re = new RegExp('wss?://', 'i');
    if (wsBaseUrl.match(re)) {
        return wsBaseUrl;
    }
    // Interpret wsBaseUrl as only path
    return window.location.origin.toString().replace('http', 'ws') + wsBaseUrl;
}

const WS_BASE_URL = generateWSUrl(); // there might be something like window.location.origin.replace('http','ws')
const RECONNECT_TIMEOUT = 2000;

interface TMessage {
    event: string
    payload: any
}

class ServerEvents {
    socket?: WebSocket;
    url: string;
    _needReconnect: boolean;
    eventsChannel: EventTarget;

    constructor(channel: string) {
        if (!WebSocket) {
            throw Error("Error! use modern browser please!");
        }
        this.url = WS_BASE_URL + channel;
        this.socket = undefined;
        this._needReconnect = true;
        this.eventsChannel = new EventTarget();
    }

    async connect() {
        this.socket = new WebSocket(this.url);
        this.socket.onclose = () => {
            setTimeout(() => {
                if (this._needReconnect) {
                    this.connect()
                }
            }, RECONNECT_TIMEOUT);
        };
        this.socket.onmessage = this.parseMessage;
        let isResolved = false;
        return new Promise((resolve, reject) => {
            if (!this.socket) {
                return reject('No socket yet');
            }
            this.socket.onopen = (event) => {
                resolve();
                isResolved = true;
            };
            this.socket.onerror = (event) => {
                if (isResolved) {
                    console.error(event);
                    return
                }
                reject(event);
            }
        });

    }

    private parseMessage = (messageEvent: MessageEvent) => {
        const message: TMessage = JSON.parse(messageEvent.data);
        this.eventsChannel.dispatchEvent(new CustomEvent(
            message.event,
            {detail: message.payload}
        ));
    };
    addEventListener(type: string, listener: any | null, options?: boolean | AddEventListenerOptions){
        this.eventsChannel.addEventListener(type, listener, options);
    }
    close() {
        if (!this.socket || this.socket.readyState !== WebSocket.OPEN) {
            return;
        }
        this.socket.onclose = null;
        this._needReconnect = false;
        this.socket.close();
    }
}

export default ServerEvents;