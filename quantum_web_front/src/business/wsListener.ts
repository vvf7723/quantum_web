import ServerEvents from "./ServerEvents";
import PlotsState from "../models/PlotsState";

const allListenersByTaskId: Record<string, any> = {};
(window as any).allListenersByTaskId = allListenersByTaskId;

export async function listenToTaskUpdates(taskId: string, state: PlotsState, setState: (state: PlotsState) => void) {
    const serverEvents = new ServerEvents('task/' + taskId)
    allListenersByTaskId[taskId] = serverEvents;
    serverEvents.addEventListener('UPDATE', (event: CustomEvent) => {
        if (event.detail.action !== 'NEW:SOLUTION' && event.detail.action !== 'DONE') {
            return
        }
        setState(state.addServerData(event.detail));
        if( event.detail.action === 'DONE'){
            serverEvents.close();
        }
    });
    await serverEvents.connect();
}