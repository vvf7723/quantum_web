import ServerMessagePayload from "./ServerMessagePayload";

export interface PlotData {
    x: number,
    y: number
}

export interface Spins {
    time: number,
    vector: number[]
}

export default class PlotsState {
    plotData: PlotData[]
    spins: Spins[];
    isDone: boolean;

    constructor(plotData?: PlotData[], spins?: Spins[], isDone? : boolean) {
        this.plotData = plotData || [];
        this.spins = spins || [];
        this.isDone = Boolean(isDone);
    }

    addServerData({timeFromStart, energy, spins, action}: ServerMessagePayload) {
        this.plotData.push({
            x: timeFromStart,
            y: energy || 0
        });
        this.spins.push({
            time: timeFromStart,
            vector: spins || []
        });
        return new PlotsState(this.plotData, this.spins, action === 'DONE');
    }
}