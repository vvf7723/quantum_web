export default interface ServerMessagePayload {
    spins?: number[],
    energy?: number,
    action: string,
    timeFromStart: number
}
