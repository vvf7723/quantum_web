from setuptools import setup

setup(
    name="qboard",
    version='0.0.0.2',
    packages=["qboard", "qboard.solvers"],
    install_requires=[
        "numpy==1.17.4",
    ]
)
