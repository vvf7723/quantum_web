import string
from random import choice

from django.http import JsonResponse

# Create your views here.
from django.views import View
from multiprocessing import Process
# from threading import Thread as Process


class StartCalculationsView(View):
    def post(self, *args, **kwargs):
        task_id = ''.join(choice(string.ascii_letters) for _ in range(12))
        from web_api.tasks import calculate
        process = Process(name=f'Calc-{task_id}', target=calculate, args=(task_id,), daemon=True)
        process.start()
        return JsonResponse({
            'taskId': task_id
        })

