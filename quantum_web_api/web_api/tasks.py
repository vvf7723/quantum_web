import json
import logging
from datetime import datetime
from os import environ

from qboard import solver as Solver, constants
import numpy as np
from time import sleep
from redis import StrictRedis

WS_BASE_PATH = '/ws/'

logger = logging.getLogger(__name__)


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


CB_TYPE_CODES = {
    constants.CB_TYPE_NEW_SOLUTION: 'NEW:SOLUTION',
    constants.CB_TYPE_NEW_LOSS: 'NEW:LOSS',
    constants.CB_TYPE_INTERRUPT_TIMEOUT: 'INT:TO',
    constants.CB_TYPE_INTERRUPT_TARGET: 'INT:TARGET',
    constants.CB_TYPE_INTERRUPT_INTERVAL: "INT:INT"
}


def calculate(task_id):
    redis = StrictRedis(db=5, host=environ.get('REDIS_HOST', '127.0.0.1'))
    logger.debug(f'Start calc with taskId={task_id}, redis - {redis}')
    sleep(1)
    start_time = datetime.now()
    matrix_q = np.random.rand(30, 30) - 0.5

    solver = Solver(mode="bf")

    def notify(payload):
        message = json.dumps({
            "event": 'UPDATE',
            "payload": payload,
        })
        channel = 'task/' + task_id
        redis.publish(WS_BASE_PATH + channel, message)

    def cb(intermediate_result: dict):
        intermediate_result['action'] = CB_TYPE_CODES.get(intermediate_result["cb_type"], 'NOP')
        intermediate_result['timeFromStart'] = (datetime.now() - start_time).total_seconds()
        for k in list(intermediate_result.keys()):
            if k.startswith('_'):
                del intermediate_result[k]
        if 'spins' in intermediate_result:
            intermediate_result['spins'] = intermediate_result['spins'].tolist()
        notify(intermediate_result)

    spins, energy = solver.solve_qubo(matrix_q, callback=cb, timeout=30, verbosity=0)
    notify({
        'spins': spins.tolist(),
        'energy': energy,
        'timeFromStart': (datetime.now() - start_time).total_seconds(),
        'action': 'DONE'
    })
    logger.debug(f'Done calc with taskId={task_id}, energy={energy}, spins={spins}')
