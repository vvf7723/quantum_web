from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from web_api.views import StartCalculationsView

urlpatterns = [
    path('start', csrf_exempt(StartCalculationsView.as_view()), name='start')
]
