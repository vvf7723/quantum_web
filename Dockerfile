FROM python:3.7-slim
LABEL title=quantum_web Tags=webserver
ENV PYTHONUNBUFFERED 1
RUN mkdir /sdk-mock
WORKDIR /sdk-mock
COPY sdk-mock /sdk-mock
RUN pip install --no-cache-dir numpy==1.17.4 scipy && python setup.py install && rm -rf /sdk-mock
RUN mkdir /code
WORKDIR /code
COPY quantum_web_api /code
RUN pip install -r requirements.txt && pip install gunicorn
# RUN python manage.py collectstatic
#CMD ["python", "manage.py", "runserver"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--timeout", "180", "--max-requests", "0", "quantum_web_api.wsgi:application"]